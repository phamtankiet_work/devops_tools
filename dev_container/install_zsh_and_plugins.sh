#!/bin/sh
# install zsh
curl -L http://install.ohmyz.sh/ | sh
chsh -s $(which zsh)

# install plugins
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k && \
git clone https://github.com/marlonrichert/zsh-autocomplete.git ${ZSH_CUSTOM:$HOME/.oh-my-zsh/custom}/plugins/zsh-autocomplete && \
git clone https://github.com/zsh-users/zsh-autosuggestions.git ${ZSH_CUSTOM:$HOME/.oh-my-zsh/custom}/plugins/zsh-autosuggestions && \
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:$HOME/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting && \
git clone https://github.com/agkozak/zsh-z $ZSH_CUSTOM/plugins/zsh-z 

# move config
cp .zshrc /root/.zshrc
cp .p10k.zsh /root/.p10k.zsh
