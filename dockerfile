FROM ubuntu:20.04

# Set environment variables
ENV DEBIAN_FRONTEND=noninteractive

# Update package list and install necessary packages
RUN apt-get update && \
    apt-get install -y \
    curl \
    unzip \
    wget \
    git \
    zsh \
    jq \
    python3-pip \
    apt-transport-https \
    ca-certificates \
    gnupg \
    lsb-release \
    software-properties-common

# Install AWS CLI
RUN pip3 install --upgrade awscli

# Install kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" && \
    install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Install Helm
RUN curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

# Install Terraform
RUN wget https://releases.hashicorp.com/terraform/$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')/terraform_$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')_linux_amd64.zip && \
    unzip terraform_$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')_linux_amd64.zip && \
    mv terraform /usr/local/bin/ && \
    rm terraform_$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')_linux_amd64.zip

# Install Ansible
RUN apt-add-repository --yes --update ppa:ansible/ansible && \
    apt-get install -y ansible

# Install zsh and powerlevel10k theme
RUN git clone --depth=1 https://github.com/romkatv/powerlevel10k.git /root/powerlevel10k && \
    echo 'source /root/powerlevel10k/powerlevel10k.zsh-theme' >> /root/.zshrc && \
    git clone https://github.com/marlonrichert/zsh-autocomplete.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autocomplete && \
    echo 'plugins+=(zsh-autocomplete)' >> /root/.zshrc && \
    git clone https://github.com/zsh-users/zsh-autosuggestions.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions && \
    echo 'plugins+=(zsh-autosuggestions)' >> /root/.zshrc && \
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting && \
    echo 'plugins+=(zsh-syntax-highlighting)' >> /root/.zshrc && \
    git clone https://github.com/agkozak/zsh-z $ZSH_CUSTOM/plugins/zsh-z && \
    echo 'plugins+=(zsh-z)' >> /root/.zshrc

COPY .p10k.zsh /root/.p10k.zsh
COPY .zshrc /root/.zshrc

# Set zsh as the default shell
SHELL ["/bin/zsh", "-c"]

# Set the working directory
WORKDIR /root

# Set the entry point
ENTRYPOINT ["/bin/zsh"]
